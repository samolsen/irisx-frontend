module.exports = {
  staticFileGlobs: [
    'manifest.json',
    'index.css',
    'index.html',

    'config/ConfigIRISx.js',
	'config/brand.css',
    'src/components/css/lib/reset.css.js',
    'src/components/css/lib/bbuttonbase.css.js',
    'src/components/css/shared.css.js',
    'src/components/irisx-app.js',

    'src/components/irisx-login/login.css.js',
    'src/components/irisx-login/irisx-login.js'
  ],
  runtimeCaching: [
    {
      urlPattern: /\/@webcomponents\/webcomponentsjs\//,
      handler: 'fastest'
    },
    {
      urlPattern: /^https:\/\/fonts.gstatic.com\//,
      handler: 'fastest'
    }
  ]
}
