import {html} from '@polymer/lit-element'
export default html`<style>
    /*<app.css>*/
    .loading-modal {
      display: none;
      justify-content: center;
      align-items: center;
      position: fixed;
      z-index: 2525;
      top: 0px;
      left: 0px;
      width: 100vw;
      height: 100vh;
      cursor: auto;
      pointer-events: all;
      background: rgba(255, 255, 255, 0.6); }
      .loading-modal.show {
        display: flex; }
    
    .animation-container {
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      padding: calc(var(--ggutter) * 2);
      border: 0px solid transparent;
      border-radius: var(--ggutter);
      background: rgba(255, 255, 255, 0.8); }
    
    .loading-message {
      display: block;
      margin-top: var(--ggutter);
      font-weight: bold;
      color: var(--brand-grey-darkest); }
    
    .la-root {
      color: var(--brand-secondary); }
    
    /*</app.css>*/
</style>`