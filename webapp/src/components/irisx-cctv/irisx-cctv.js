import { LitElement, html } from '@polymer/lit-element'
import { store } from '../../redux/redux-store'
import { connect } from 'pwa-helpers/connect-mixin.js'
import { navigate } from 'lit-redux-router'
import { addItem, removeItem } from '../../redux/redux-LoaderAnim'
import { setMobileState, CAMERAS } from '../../redux/redux-MobileState';
import { Cameras } from '../../rest/Cameras'

import { ConfigIRISx, ConfigCCTV } from '/config/Config.js'

import { BreakpointMixin } from '../../utils/breakpoint-mixin'
import { idx, i2xy, xy2i } from '../../utils/utils'
import { Monitors } from '../../rest/Monitors.js'

import CSSReset from '../css/lib/reset.css.js'
import BButtonBase from '../css/lib/bbuttonbase.css.js'
import SharedStyles from '../css/shared.css.js'
import CCTVStyles from './cctv.css.js'

import '../common/irisx-page-nav'
import './irisx-groups/irisx-groups-current'
import './irisx-groups/irisx-groups-list'
import './irisx-sources/irisx-sources-mobile-menu'

window.customElements.get('irisx-cctv') || window.customElements.define('irisx-cctv',
class extends connect(store)(BreakpointMixin(LitElement)) {
    render() {  return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${CCTVStyles}

        <irisx-page-nav page="cctv"></irisx-page-nav>
        <main id="page-container">
            <section id="sources-container">

                <lit-route path="/cctv/:group/map">
                    <irisx-sources-map
                        group="${this.group}" view="${this.view}"
                        mobilestate="${this.mobilestate}"
                        monitorOffsetY="${this.monitorOffsetY}"
                        .activeSlot="${this.activeSlot}"
                        .cameraTypes="${this.cameraTypes}" @cameraTypesChange="${this._onCameraTypesChange}"
                        .cameras="${this.cameras}" @listCameras="${this._onListCameras}"
                        .monitor="${this.monitor}"
                        .castingCam="${this.castingCam}" @camClick="${this._onCamClick}">
                    </irisx-sources-map>
                </lit-route>

                <lit-route path="/cctv/:group/table">
                    <irisx-sources-table
                        group="${this.group}" view="${this.view}"
                        mobilestate="${this.mobilestate}"
                        .cameraTypes="${this.cameraTypes}" @cameraTypesChange="${this._onCameraTypesChange}"
                        .cameras="${this.cameras}" @listCameras="${this._onListCameras}"
                        .monitor="${this.monitor}"
                        .castingCam="${this.castingCam}" @camClick="${this._onCamClick}">
                    </irisx-sources-table>
                </lit-route>

            </section>
            <section id="groups-sidebar">

                <irisx-groups-current
                    group="${this.group}"
                    .monitor="${this.monitor}" @listMonitors="${this._onListMonitors}"
                    .activeSlot="${this.activeSlot}" @slotClick="${this._onSlotClick}"
                    @gridTypeChange="${this._onGridTypeChange}"
                    @monitorOffsetChange="${this._onMonitorOffsetChange}"
                    .castingCam="${this.castingCam}">
                </irisx-groups-current>

                <irisx-groups-list
                    group="${this.group}" view="${this.view}"
                    mobilestate="${this.mobilestate}",
                    monitorOffsetY="${this.monitorOffsetY}"
                    .monitors="${this.monitors}">
                </irisx-groups-list>

            </section>
            <section id="mobile-menu">
                <irisx-sources-mobile-menu
                    group="${this.group}" view="${this.view}"
                    mobilestate="${this.mobilestate}">
                </irisx-sources-mobile-menu>
            </section>
        </main>
    `}

    static get properties() {   return {
            //  routing stuff
        page:{type:String},
        view:{type:String},
        group:{type:String},
        mobilestate:{type:String},
        currentRoute:{type:String},
        firstMobileView:{type:Boolean},
            //  need this to maintain scalable sidebar layout stuff
        monitorOffsetX:{type:Number,reflet:true},
        monitorOffsetY:{type:Number,reflet:true},
            //  model stuff, should probably pull this out into global state management eventually
        monitors:{type:Array},
        monitor:{type:Object},
        cameras:{type:Array},
        cameraTypes:{type:Array},
        activeSlot:{type:Number},
        castingCam:{type:String},
        camRefreshInterval:{type:Number},
        gridChangePreventRefresh:{type:Boolean}

    }}

    constructor(...args){  super(...args)
        this.mobileMapOffset = 0
        store.dispatch(setMobileState(CAMERAS))
        this.firstMobileView = true
        this.gridChangePreventRefresh = false

            //  data models

        this.monitors = Monitors.mockMonitors
        this.monitor = Monitors.mockMonitor
        this._onRefreshMonitors = this._onRefreshMonitors.bind(this)
        this.monitorRefreshInterval = setInterval(this._onRefreshMonitors,ConfigCCTV.MonitorsPollingRate)
        //  this._getMonitors() //  initial page routing should trigger this

        this.cameras = Cameras.mockCameras
        this._onRefreshCameras = this._onRefreshCameras.bind(this)
        this.camRefreshInterval = setInterval(this._onRefreshCameras,ConfigCCTV.CameraPollingRate)
        let savedCameraTypes = window.sessionStorage.getItem('IRISx_cameraTypes')
        if(savedCameraTypes === null){
            this.cameraTypes = ['CCTV']
        }else{
            this.cameraTypes = JSON.parse(savedCameraTypes)
        }
        this._getCameras()
    }

    updated(props){
        if(props.has('cameraTypes'))    window.sessionStorage.setItem('IRISx_cameraTypes',JSON.stringify(this.cameraTypes))
    }

    stateChanged(state){

            //  ROUTING

        if(     //  new state includes routing data
            idx(['router','activeRoute'],state) &&
                //  route applies to cctv page
            state.router.activeRoute.indexOf('cctv') !== -1 &&
                //  and route isn't a duplicate of previous
            state.router.activeRoute !== this.currentRoute
        ){
            this.currentRoute = state.router.activeRoute
            let routeSegments = this.currentRoute.split('/cctv').pop().split('/')
            let _group = routeSegments[1] || ''
            let _view = routeSegments[2] || ''

            if(this.group!== _group){
                this.castingCam = undefined
                this.activeSlot = undefined
            }

            this.group = _group
            this._getMonitors()

                //  view routing

            if(this.view !== _view){
                switch(_view){
                    case 'map':
                        if(this.isMobile)   this.firstMobileView = false
                        this.view = _view
                        import('./irisx-sources/irisx-sources-map.js')
                    case 'table':
                        this.view = _view
                        import('./irisx-sources/irisx-sources-table.js')
                    break
                        //  default view
                    case '':
                    default:
                        //  console.error('not a valid view:',_view)
                        let defaultViewSegment = idx(['Pages','cctv','defaultRouteSegments','view',this.currentBreakpointName],ConfigIRISx) || 'map'    // if all else fails, default to map.
                        if(defaultViewSegment===''){
                            console.error(`unrecognized view id:'${_view}', failed to find default...`)
                        }else{
                            console.warn(`unrecognized view id:'${_view}', defaulting to '${defaultViewSegment}'`)
                            return store.dispatch(navigate(`/cctv/${_group}/${defaultViewSegment}`))
                        }
                    break
                }
            }
        }

            //  MOBILE STATE

            if(     //  new state includes mobilestate data
                idx(['MobileState','state'],state) &&
                    //  and itsn't a dupe
                state.MobileState.state !== this.mobilestate
            ){
                this.mobilestate = state.MobileState.state
            }
    }

    _onCameraTypesChange(e){
        this.cameraTypes = e.cameraTypes
        this._getCameras()
    }

    _onRefreshCameras(){
        if(
            this.page === 'cctv' &&
            this.activeSlot === undefined &&
            this.castingCam === undefined
        ){
            this._getCameras(false)
        }
    }
    _onListCameras(){  this._getCameras() }
    _getCameras(showLoader=true){
        if(showLoader)  store.dispatch(addItem(this,'list-cameras'))
        Cameras.get(this.cameraTypes).then(response=>{
            if(showLoader)  store.dispatch(removeItem(this,'list-cameras'))
            if(response.response.status === 200 && response.response.ok === true){
                this.cameras = response.data.map(camera=>{
                    if(camera.displayName === null || camera.displayName === '')    camera.displayName = camera.name
                    return camera
                })
            }else if(response.response.status === 401){
                console.warn('missing authorization, redirecting to login page...')
                return store.dispatch(navigate('/login'))
            }else{
                console.error('could not load cameras:',response)
            }
        })
    }

    _onRefreshMonitors(){
            //  refresh monitor list when
        if(
                //  you're on the cctv page
            this.page === 'cctv' &&
                //  and you're not currently trying to fill a slot
            this.activeSlot === undefined &&
                //  and there's no casting cam selected
            this.castingCam === undefined &&
                //  and you didn't just change the grid layout
            this.gridChangePreventRefresh === false
        ){
            this._getMonitors(this.group,false)
        }
    }
    _onListMonitors(e){ this._getMonitors(e.controllerName)  }
    _getMonitors(controllerName,showLoader=true){
        if(showLoader)  store.dispatch(addItem(this,'list-monitors'))
        Monitors.get(controllerName).then(response=>{
            if(showLoader)  store.dispatch(removeItem(this,'list-monitors'))
            if(response.response.status === 200 && response.response.ok === true){
                if(Array.isArray(response.data)){
                    this.monitors = response.data.map(monitor=>{
                        if(monitor.displayName === null || monitor.displayName === '')    monitor.displayName = monitor.name
                        return monitor
                    })
                }else{
                    this.monitors.forEach((_monitor,index,_monitors)=>{
                        if(_monitor.name === response.data.name){
                            _monitors[index] = response.data
                        }
                    })
                }
                this._setMonitor()
            }else if(response.response.status === 401){
                console.warn('missing authorization, redirecting to login page...')
                return store.dispatch(navigate('/login'))
            }else{
                console.error('could not load monitors:',response)
            }
        })
    }

    _setMonitor(){
        let _monitor = this.monitors.find((monitor)=>monitor.name===this.group)
        if(!_monitor){
            let defaultGroupSegment = idx(['Pages','cctv','defaultRouteSegments','group',this.currentBreakpointName],ConfigIRISx) || this.monitors[0].name
            if(defaultGroupSegment==='' || defaultGroupSegment===undefined){
                if(this.monitors[0].name === ''){
                    console.warn(`'${this.group}' not found in unpopulated monitor list...`)
                }else{
                    console.error(`unrecognized monitor name:'${this.group}', failed to find default...`)
                }
            }else{
                console.warn(`unrecognized monitor name:'${this.group}', defaulting to '${defaultGroupSegment}'`)
                return store.dispatch(navigate(`/cctv/${defaultGroupSegment}/${this.view}`))
            }
        }else{  //  everything is probably okay
            this.monitor = _monitor
        }
    }

    _onGridTypeChange(e){
        this.gridChangePreventRefresh = true
        let _monitor = Object.assign({},this.monitor,{attachedCameras:new Array(e.gridType)})

        let newTotal = e.gridType
        let newSize = Math.sqrt(newTotal)
        let oldTotal = this.monitor.attachedCameras.length
        let oldSize = Math.sqrt(oldTotal)

        for(let i = 0; i < e.gridType; i++){
            let newXY = i2xy(i,newSize)            
            if(newXY[0] < oldSize && newXY[1] < oldSize){   //  if the new coords fall within the old grid
                let oldIndex = xy2i(newXY[0],newXY[1],oldSize)
                let oldCam = this.monitor.attachedCameras[oldIndex]
                _monitor.attachedCameras[i] = oldCam || ''
            }else{
                _monitor.attachedCameras[i] = ''
            }
        }
        this.monitor = _monitor
    }
    
    _onSlotClick(e){
        if(this.isMobile){
            store.dispatch(setMobileState(CAMERAS))
                //  by default, slots are populated from the table - *unless* they've already clicked through to the map and back
            if(this.firstMobileView === true && e.group === 'map')   return store.dispatch(navigate(`/cctv/${e.group}/map`))
        }
        //  console.log('active slot: group',e.group,'slot',e.slot)  //  activeSlot

        this.activeSlot = (this.activeSlot !== e.slot) ? e.slot : undefined
        this._setCastingCam((this.activeSlot) ? this.monitor.attachedCameras[e.slot] : undefined,false)
    }

    _onCamClick(e){ this._setCastingCam(e.camera,true)  }

    _setCastingCam(_castingCam,castable){
        //  console.log(this,'considering cam',_castingCam)
        if(this.activeSlot === undefined){
            this.castingCam = undefined
        /*
        }else if(this.castingCam === _castingCam){
            //  NOTHING!!!
        */
        }else{
            this.castingCam = _castingCam
            if(castable){
                //  console.log('precast',this.monitor.attachedCameras)
                this.monitor.attachedCameras[this.activeSlot] = _castingCam
                store.dispatch(addItem(this,'set-monitor'))
                Monitors.set(this.group,this.monitor.attachedCameras).then(response=>{
                    store.dispatch(removeItem(this,'set-monitor'))
                    if(response.response.status === 200 && response.response.ok === true){
                        this.monitor = response.data
                        this.gridChangePreventRefresh = false
                    }else if(response.response.status === 401){
                        console.warn('missing authorization, redirecting to login page...')
                        return store.dispatch(navigate('/login'))
                    }else{
                        console.error('could not load monitors:',response)
                    }
                })
            }
        }
        //  console.log(this,'decided to set cam to',this.castingCam)
    }
    
    _onMonitorOffsetChange(e){
        this.monitorOffsetX = e.monitorOffsetX
        this.monitorOffsetY = e.monitorOffsetY
    }
})