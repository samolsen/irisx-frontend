import {html} from '@polymer/lit-element'
export default html`<style>
    /*<cctv.css>*/
    #page-container {
      display: flex;
      align-items: stretch;
      height: 100%; }
    
    #sources-container {
      flex: 1 0 auto;
      border-right: 2px solid var(--brand-grey-light); }
    
    @media (min-width: 820px) and (max-width: 1129px) {
      #sources-container {
        flex: 0 0 460px;
        width: 460px;
        min-width: 460px; } }
    
    #groups-sidebar {
      flex: 0 1 650px;
      box-sizing: border-box;
      color: var(--brand-grey-dark);
      background: white; }
    
    @media (min-width: 820px) and (max-width: 1129px) {
      #groups-sidebar {
        flex: 1 1 calc(100% - 460px);
        width: calc(100% - 460px);
        margin-top: 84px; } }
    
    #mobile-menu {
      display: none;
      order: 2;
      position: fixed;
      bottom: 0px;
      left: 0px;
      z-index: 1010;
      width: 100%;
      height: 60px;
      background-color: var(--brand-grey-light); }
    
    @media (max-width: 819px) {
      #mobile-menu {
        display: block; }
      #page-container {
        display: flex;
        justify-content: flex-start;
        flex-direction: column;
        align-items: unset;
        width: 100vw;
        height: 100vh;
        overflow-x: hidden; }
      #groups-sidebar {
        width: 100%;
        flex: 0 0 0;
        order: 0; }
      #sources-container {
        display: block;
        width: 100%;
        flex-grow: 1;
        flex-shrink: 0;
        order: 1;
        border-top: 1px solid var(--brand-grey-light);
        padding-bottom: 60px; } }
    
    /*</cctv.css>*/
</style>`