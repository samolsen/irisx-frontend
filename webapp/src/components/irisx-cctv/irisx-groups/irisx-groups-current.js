import { LitElement, html } from '@polymer/lit-element'
import { DetailsModMixin } from '../../../utils/details-mod-mixin.js'
import { BreakpointMixin } from '../../../utils/breakpoint-mixin'

import { ConfigCCTV } from '/config/Config.js'
import { Monitors } from '../../../rest/Monitors.js'

import CSSReset from '../../css/lib/reset.css.js'
import BButtonBase from '../../css/lib/bbuttonbase.css.js'
import SharedStyles from '../../css/shared.css.js'
import GroupsCurrentStyles from './groupscurrent.css.js'
import CustomComposedEvent from '../../../utils/CustomComposedEvent.js'
import { StringIfPropIsVal, Round2Sq } from '../../../utils/utils.js'

import './grid-icon.js'

window.customElements.get('irisx-groups-current') || window.customElements.define('irisx-groups-current', 
class extends BreakpointMixin(DetailsModMixin(LitElement)) {
    render() {  return html`

        ${CSSReset}
        ${BButtonBase}
        ${SharedStyles}
        ${GroupsCurrentStyles}

        <header id="group-container">
            <figure>
                <figcaption>
                    <form class="monitor-grid-select" @change="${this._gridTypeChange}">
                        <details class="modal-overlay details-mod" @toggle="${this._toggleDetails}"><summary><grid-icon count="${this.slots.length}"></grid-icon></summary>
                            <div class="details-content">
                                <legend>Layout</legend>
                                <ol>${ConfigCCTV.Grids.map(i=>html`
                                    <li><label><input type="radio" name="grid-type" value="${i}" .checked="${(this.slots.length===i) ? true : false}" /><grid-icon count="${i}" state="${StringIfPropIsVal('active',this.slots.length,i)}"></grid-icon><span>${i}</span></label></li>
                                `)}</ol>
                                <button type="button" class="bbuttonbase close-btn" title="close"><img src="img/icon-close-fill.svg" alt="close" width="12" height="12" /></button>
                            </div>
                        </details>
                    </form>
                    <h2 id="group-title">${this.monitor.displayName || this.monitor.name || this.monitor.controllerName}</h2>
                </figcaption>
                <ol class="display-slots grid-${this.slots.length}up">${this.slots.map((slot,i)=>html`
                    <li slot="${this.activeSlot}" index="${i}" 
                        class="${StringIfPropIsVal('active',parseInt(this.activeSlot),i)}"><button class="bbuttonbase"
                        value="${i}"
                        @click="${this._slotClicked}"
                        title="${(slot.id !== null) ? `camera ${slot.id}` : `slot #${i+1} (empty)`}"
                    >${(slot.id !== null) ?
                        html`<img src="${this._getThumbByCamID(slot.id)}" alt="camera ${slot.id}" @load="${this._gridImageLoaded}" />` :
                        html`<div class="empty-grid" title="slot #${i+1} (empty)"><span>${i+1}</span></div>`
                    }</button></li>
                `)}</ol>
            </figure>
        </header>
    `}

    static get properties(){    return {
            //  routing
        group:{type:String,reflect:true},
        view:{type:String,reflect:true},
            //  positioning
        groupContainer:{type:HTMLElement},
        monitorOffsetX:{type:Number},
        monitorOffsetY:{type:Number},
            //  view
        monitor:{type:Object},
        slots:{type:Array},
            // casting
        activeSlot:{type:Number},
        castingCam:{type:String}
    }}

    constructor(){  super()
        this.monitor = Monitors.mockMonitor
        this._setSlots()
    }

    firstUpdated(){
        this.groupContainer = this.shadowRoot.getElementById('group-container')
        this._monitorOffsetChange()
    }

    updated(props){
        if(props.has('monitor')){
            //  console.log(this.monitor)
            this._setSlots(this.monitor.attachedCameras.length)
            this._monitorOffsetChange()
        }
        if(props.has('group')){
            this._monitorOffsetChange()
        }
    }

    _setSlots(count){
        let _slots = []
        for(let i = 0; i < Round2Sq(count); i++){
            _slots.push({id:this.monitor.attachedCameras[i] || null})
        }
        this.slots = _slots
    }

    _getThumbByCamID(id){   return ConfigCCTV.CamThumbRoot+id+ConfigCCTV.CamThumbExt    }

    _gridImageLoaded(){
        this._monitorOffsetChange()
    }

    _breakpointHandler(e){ super._breakpointHandler(e)
        this._monitorOffsetChange()
    }

    _monitorOffsetChange(){
        if(this.groupContainer){
            let _monitorOffsetX = this.groupContainer.offsetWidth
            let _monitorOffsetY = this.groupContainer.offsetHeight
            if(this.monitorOffsetX !== _monitorOffsetX || this.monitorOffsetY !== _monitorOffsetY){
                this.dispatchEvent(new CustomComposedEvent('monitorOffsetChange',{
                    monitorOffsetX:this.monitorOffsetX = _monitorOffsetX,
                    monitorOffsetY:this.monitorOffsetY = _monitorOffsetY
                }))
            }
        }
    }

        //  copy grid type icon and close menu

    _toggleDetails(e){
        e.target.querySelector('summary>grid-icon').setAttribute('state',(e.target.open) ? 'active' : null )
    }

    _gridTypeChange(e){

        Array.from(e.target.form[e.target.name]).forEach(input=>{
            let adjacentGridIcon = input.parentElement.querySelector('grid-icon')
            if(input.checked){
                adjacentGridIcon.setAttribute('state','active')
            }else{
                adjacentGridIcon.removeAttribute('state')
            }
        })

        let details = e.target.form.querySelector('details')
        let count = e.target.parentElement.querySelector('grid-icon').count
        let roundedCount = Round2Sq(count)
        details.querySelector('summary>grid-icon').count = roundedCount
        this.dispatchEvent(new CustomComposedEvent('gridTypeChange',{
            group:this.group,
            gridType:roundedCount
        }))
        this.dispatchEvent(new CustomComposedEvent('slotClick',{
            group:this.group,
            slot:undefined
        }))
        details.open = false
    }

    _slotClicked(e){
        this.dispatchEvent(new CustomComposedEvent('slotClick',{
            group:this.group,
            slot:e.currentTarget.value
        }))
    }
})