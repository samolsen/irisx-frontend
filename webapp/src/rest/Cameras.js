import { ConfigAPI } from '/config/Config.js'
import { Authorization } from '../rest/Authorization'
export const Cameras = class {
    static async get(cameraTypes,boundingBox){ return new Promise((resolve,reject)=>{
        
        let url = new URL(ConfigAPI.CamerasEndpoint,ConfigAPI.APIRoot)

        if(cameraTypes)  cameraTypes.forEach(type=>url.searchParams.append('cameraTypes',type))    //  append new entry for each item
        if(boundingBox)	url.searchParams.set('boundingBox',JSON.stringify(boundingBox)) //  json passed through query string params
        //  url.searchParams.set('cacheBuster',Date.now())
        
        let request = new Request(url,{
            method:'GET',
            headers:new Headers({
                'Authorization':Authorization.JWT,
                'Cache-Control':'no-store'
            })
        })

        fetch(request).then(response=>{
            response.json().then(data=>{
                resolve({response:response,data:data})
            },error=>reject(error))
        },error=>reject(error))
    })}

    static get mockCameras(){   return [{
        "cameraType": "CCTV",
        "displayName": "string",
        "lat": 0,
        "lon": 0,
        "name": "string",
        "status": "PUBLISHED",
        "mock":true
    }]}
}