import { createStore, compose, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { lazyReducerEnhancer } from 'pwa-helpers/lazy-reducer-enhancer.js'
import { LoaderAnim } from './redux-LoaderAnim.js'
import { MobileState } from './redux-MobileState.js'

const devCompose = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose //  for time travel debugging

export const store = createStore(state=>state, devCompose(lazyReducerEnhancer(combineReducers),applyMiddleware(thunk)))
store.addReducers({LoaderAnim,MobileState})
