/* ⚠    KEEP THIS FILE! If you need to make changes, copy and rename this default version for future reference!   ⚠ */

window.__IRISXConfig.API = {
		//	AUTH
    APIRoot:'https://dev.irisx.org/api/',
	AuthEndpoint:'auth/signin',	//	POST
		//	CAMERAS
	CamerasEndpoint:'cameras',	//	GET
		//	MONSTREAM CONTROLLERS
	getMonitorsEndpoint(name){return `controllers/${name}`},	//	GET
	getSetMonitorEndpoint(name){return `controllers/${name}/cameras`}	//	POST
}